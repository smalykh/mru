;; Auto-generated. Do not edit!


(when (boundp 'lecture2_actions::MoveFeedback)
  (if (not (find-package "LECTURE2_ACTIONS"))
    (make-package "LECTURE2_ACTIONS"))
  (shadow 'MoveFeedback (find-package "LECTURE2_ACTIONS")))
(unless (find-package "LECTURE2_ACTIONS::MOVEFEEDBACK")
  (make-package "LECTURE2_ACTIONS::MOVEFEEDBACK"))

(in-package "ROS")
;;//! \htmlinclude MoveFeedback.msg.html


(defclass lecture2_actions::MoveFeedback
  :super ros::object
  :slots (_position _speed _elapsed _remains ))

(defmethod lecture2_actions::MoveFeedback
  (:init
   (&key
    ((:position __position) 0.0)
    ((:speed __speed) 0.0)
    ((:elapsed __elapsed) 0.0)
    ((:remains __remains) 0.0)
    )
   (send-super :init)
   (setq _position (float __position))
   (setq _speed (float __speed))
   (setq _elapsed (float __elapsed))
   (setq _remains (float __remains))
   self)
  (:position
   (&optional __position)
   (if __position (setq _position __position)) _position)
  (:speed
   (&optional __speed)
   (if __speed (setq _speed __speed)) _speed)
  (:elapsed
   (&optional __elapsed)
   (if __elapsed (setq _elapsed __elapsed)) _elapsed)
  (:remains
   (&optional __remains)
   (if __remains (setq _remains __remains)) _remains)
  (:serialization-length
   ()
   (+
    ;; float64 _position
    8
    ;; float64 _speed
    8
    ;; float64 _elapsed
    8
    ;; float64 _remains
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _position
       (sys::poke _position (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _speed
       (sys::poke _speed (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _elapsed
       (sys::poke _elapsed (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _remains
       (sys::poke _remains (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _position
     (setq _position (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _speed
     (setq _speed (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _elapsed
     (setq _elapsed (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _remains
     (setq _remains (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get lecture2_actions::MoveFeedback :md5sum-) "41e7322a967ad4991202fe695603a346")
(setf (get lecture2_actions::MoveFeedback :datatype-) "lecture2_actions/MoveFeedback")
(setf (get lecture2_actions::MoveFeedback :definition-)
      "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======
# feedback
float64 position
float64 speed
float64 elapsed
float64 remains


")



(provide :lecture2_actions/MoveFeedback "41e7322a967ad4991202fe695603a346")


