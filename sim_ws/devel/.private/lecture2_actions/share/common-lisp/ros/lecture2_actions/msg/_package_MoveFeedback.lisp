(cl:in-package lecture2_actions-msg)
(cl:export '(POSITION-VAL
          POSITION
          SPEED-VAL
          SPEED
          ELAPSED-VAL
          ELAPSED
          REMAINS-VAL
          REMAINS
))