
"use strict";

let MoveAction = require('./MoveAction.js');
let MoveActionGoal = require('./MoveActionGoal.js');
let MoveFeedback = require('./MoveFeedback.js');
let MoveGoal = require('./MoveGoal.js');
let MoveActionResult = require('./MoveActionResult.js');
let MoveActionFeedback = require('./MoveActionFeedback.js');
let MoveResult = require('./MoveResult.js');

module.exports = {
  MoveAction: MoveAction,
  MoveActionGoal: MoveActionGoal,
  MoveFeedback: MoveFeedback,
  MoveGoal: MoveGoal,
  MoveActionResult: MoveActionResult,
  MoveActionFeedback: MoveActionFeedback,
  MoveResult: MoveResult,
};
