;; Auto-generated. Do not edit!


(when (boundp 'lecture1_msgs::TimeUInt32)
  (if (not (find-package "LECTURE1_MSGS"))
    (make-package "LECTURE1_MSGS"))
  (shadow 'TimeUInt32 (find-package "LECTURE1_MSGS")))
(unless (find-package "LECTURE1_MSGS::TIMEUINT32")
  (make-package "LECTURE1_MSGS::TIMEUINT32"))

(in-package "ROS")
;;//! \htmlinclude TimeUInt32.msg.html


(defclass lecture1_msgs::TimeUInt32
  :super ros::object
  :slots (_stamp _data ))

(defmethod lecture1_msgs::TimeUInt32
  (:init
   (&key
    ((:stamp __stamp) (instance ros::time :init))
    ((:data __data) 0)
    )
   (send-super :init)
   (setq _stamp __stamp)
   (setq _data (round __data))
   self)
  (:stamp
   (&optional __stamp)
   (if __stamp (setq _stamp __stamp)) _stamp)
  (:data
   (&optional __data)
   (if __data (setq _data __data)) _data)
  (:serialization-length
   ()
   (+
    ;; time _stamp
    8
    ;; uint32 _data
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; time _stamp
       (write-long (send _stamp :sec) s) (write-long (send _stamp :nsec) s)
     ;; uint32 _data
       (write-long _data s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; time _stamp
     (send _stamp :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _stamp :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint32 _data
     (setq _data (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get lecture1_msgs::TimeUInt32 :md5sum-) "a92d2e7b146feacea180959ae4c653b5")
(setf (get lecture1_msgs::TimeUInt32 :datatype-) "lecture1_msgs/TimeUInt32")
(setf (get lecture1_msgs::TimeUInt32 :definition-)
      "# Time stamp
time stamp
# Data
uint32 data


")



(provide :lecture1_msgs/TimeUInt32 "a92d2e7b146feacea180959ae4c653b5")


