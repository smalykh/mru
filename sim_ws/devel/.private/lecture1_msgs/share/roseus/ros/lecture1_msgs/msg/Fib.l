;; Auto-generated. Do not edit!


(when (boundp 'lecture1_msgs::Fib)
  (if (not (find-package "LECTURE1_MSGS"))
    (make-package "LECTURE1_MSGS"))
  (shadow 'Fib (find-package "LECTURE1_MSGS")))
(unless (find-package "LECTURE1_MSGS::FIB")
  (make-package "LECTURE1_MSGS::FIB"))

(in-package "ROS")
;;//! \htmlinclude Fib.msg.html


(defclass lecture1_msgs::Fib
  :super ros::object
  :slots (_data ))

(defmethod lecture1_msgs::Fib
  (:init
   (&key
    ((:data __data) 0)
    )
   (send-super :init)
   (setq _data (round __data))
   self)
  (:data
   (&optional __data)
   (if __data (setq _data __data)) _data)
  (:serialization-length
   ()
   (+
    ;; uint32 _data
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint32 _data
       (write-long _data s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint32 _data
     (setq _data (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get lecture1_msgs::Fib :md5sum-) "304a39449588c7f8ce2df6e8001c5fce")
(setf (get lecture1_msgs::Fib :datatype-) "lecture1_msgs/Fib")
(setf (get lecture1_msgs::Fib :definition-)
      "# Data
uint32 data


")



(provide :lecture1_msgs/Fib "304a39449588c7f8ce2df6e8001c5fce")


