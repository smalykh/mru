
(cl:in-package :asdf)

(defsystem "lecture1_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Fib" :depends-on ("_package_Fib"))
    (:file "_package_Fib" :depends-on ("_package"))
  ))