; Auto-generated. Do not edit!


(cl:in-package lecture1_msgs-msg)


;//! \htmlinclude Fib.msg.html

(cl:defclass <Fib> (roslisp-msg-protocol:ros-message)
  ((data
    :reader data
    :initarg :data
    :type cl:integer
    :initform 0))
)

(cl:defclass Fib (<Fib>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Fib>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Fib)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name lecture1_msgs-msg:<Fib> is deprecated: use lecture1_msgs-msg:Fib instead.")))

(cl:ensure-generic-function 'data-val :lambda-list '(m))
(cl:defmethod data-val ((m <Fib>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lecture1_msgs-msg:data-val is deprecated.  Use lecture1_msgs-msg:data instead.")
  (data m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Fib>) ostream)
  "Serializes a message object of type '<Fib>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'data)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'data)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'data)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Fib>) istream)
  "Deserializes a message object of type '<Fib>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'data)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'data)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'data)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Fib>)))
  "Returns string type for a message object of type '<Fib>"
  "lecture1_msgs/Fib")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Fib)))
  "Returns string type for a message object of type 'Fib"
  "lecture1_msgs/Fib")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Fib>)))
  "Returns md5sum for a message object of type '<Fib>"
  "304a39449588c7f8ce2df6e8001c5fce")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Fib)))
  "Returns md5sum for a message object of type 'Fib"
  "304a39449588c7f8ce2df6e8001c5fce")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Fib>)))
  "Returns full string definition for message of type '<Fib>"
  (cl:format cl:nil "# Data~%uint32 data~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Fib)))
  "Returns full string definition for message of type 'Fib"
  (cl:format cl:nil "# Data~%uint32 data~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Fib>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Fib>))
  "Converts a ROS message object to a list"
  (cl:list 'Fib
    (cl:cons ':data (data msg))
))
