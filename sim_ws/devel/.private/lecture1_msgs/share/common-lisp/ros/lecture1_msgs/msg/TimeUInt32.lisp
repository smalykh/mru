; Auto-generated. Do not edit!


(cl:in-package lecture1_msgs-msg)


;//! \htmlinclude TimeUInt32.msg.html

(cl:defclass <TimeUInt32> (roslisp-msg-protocol:ros-message)
  ((stamp
    :reader stamp
    :initarg :stamp
    :type cl:real
    :initform 0)
   (data
    :reader data
    :initarg :data
    :type cl:integer
    :initform 0))
)

(cl:defclass TimeUInt32 (<TimeUInt32>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <TimeUInt32>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'TimeUInt32)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name lecture1_msgs-msg:<TimeUInt32> is deprecated: use lecture1_msgs-msg:TimeUInt32 instead.")))

(cl:ensure-generic-function 'stamp-val :lambda-list '(m))
(cl:defmethod stamp-val ((m <TimeUInt32>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lecture1_msgs-msg:stamp-val is deprecated.  Use lecture1_msgs-msg:stamp instead.")
  (stamp m))

(cl:ensure-generic-function 'data-val :lambda-list '(m))
(cl:defmethod data-val ((m <TimeUInt32>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lecture1_msgs-msg:data-val is deprecated.  Use lecture1_msgs-msg:data instead.")
  (data m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <TimeUInt32>) ostream)
  "Serializes a message object of type '<TimeUInt32>"
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'stamp)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'stamp) (cl:floor (cl:slot-value msg 'stamp)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'data)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'data)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'data)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <TimeUInt32>) istream)
  "Deserializes a message object of type '<TimeUInt32>"
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'stamp) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'data)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'data)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'data)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<TimeUInt32>)))
  "Returns string type for a message object of type '<TimeUInt32>"
  "lecture1_msgs/TimeUInt32")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'TimeUInt32)))
  "Returns string type for a message object of type 'TimeUInt32"
  "lecture1_msgs/TimeUInt32")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<TimeUInt32>)))
  "Returns md5sum for a message object of type '<TimeUInt32>"
  "a92d2e7b146feacea180959ae4c653b5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'TimeUInt32)))
  "Returns md5sum for a message object of type 'TimeUInt32"
  "a92d2e7b146feacea180959ae4c653b5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<TimeUInt32>)))
  "Returns full string definition for message of type '<TimeUInt32>"
  (cl:format cl:nil "# Time stamp~%time stamp~%# Data~%uint32 data~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'TimeUInt32)))
  "Returns full string definition for message of type 'TimeUInt32"
  (cl:format cl:nil "# Time stamp~%time stamp~%# Data~%uint32 data~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <TimeUInt32>))
  (cl:+ 0
     8
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <TimeUInt32>))
  "Converts a ROS message object to a list"
  (cl:list 'TimeUInt32
    (cl:cons ':stamp (stamp msg))
    (cl:cons ':data (data msg))
))
