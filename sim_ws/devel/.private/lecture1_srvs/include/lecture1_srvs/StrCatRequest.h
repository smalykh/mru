// Generated by gencpp from file lecture1_srvs/StrCatRequest.msg
// DO NOT EDIT!


#ifndef LECTURE1_SRVS_MESSAGE_STRCATREQUEST_H
#define LECTURE1_SRVS_MESSAGE_STRCATREQUEST_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace lecture1_srvs
{
template <class ContainerAllocator>
struct StrCatRequest_
{
  typedef StrCatRequest_<ContainerAllocator> Type;

  StrCatRequest_()
    : first()
    , second()  {
    }
  StrCatRequest_(const ContainerAllocator& _alloc)
    : first(_alloc)
    , second(_alloc)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _first_type;
  _first_type first;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _second_type;
  _second_type second;





  typedef boost::shared_ptr< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> const> ConstPtr;

}; // struct StrCatRequest_

typedef ::lecture1_srvs::StrCatRequest_<std::allocator<void> > StrCatRequest;

typedef boost::shared_ptr< ::lecture1_srvs::StrCatRequest > StrCatRequestPtr;
typedef boost::shared_ptr< ::lecture1_srvs::StrCatRequest const> StrCatRequestConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::lecture1_srvs::StrCatRequest_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace lecture1_srvs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "c0d0db6e21f3fc1eb068f9cc22ba8beb";
  }

  static const char* value(const ::lecture1_srvs::StrCatRequest_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xc0d0db6e21f3fc1eULL;
  static const uint64_t static_value2 = 0xb068f9cc22ba8bebULL;
};

template<class ContainerAllocator>
struct DataType< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "lecture1_srvs/StrCatRequest";
  }

  static const char* value(const ::lecture1_srvs::StrCatRequest_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "string first\n\
string second\n\
";
  }

  static const char* value(const ::lecture1_srvs::StrCatRequest_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.first);
      stream.next(m.second);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct StrCatRequest_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::lecture1_srvs::StrCatRequest_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::lecture1_srvs::StrCatRequest_<ContainerAllocator>& v)
  {
    s << indent << "first: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.first);
    s << indent << "second: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.second);
  }
};

} // namespace message_operations
} // namespace ros

#endif // LECTURE1_SRVS_MESSAGE_STRCATREQUEST_H
