;; Auto-generated. Do not edit!


(when (boundp 'lecture1_srvs::StrCat)
  (if (not (find-package "LECTURE1_SRVS"))
    (make-package "LECTURE1_SRVS"))
  (shadow 'StrCat (find-package "LECTURE1_SRVS")))
(unless (find-package "LECTURE1_SRVS::STRCAT")
  (make-package "LECTURE1_SRVS::STRCAT"))
(unless (find-package "LECTURE1_SRVS::STRCATREQUEST")
  (make-package "LECTURE1_SRVS::STRCATREQUEST"))
(unless (find-package "LECTURE1_SRVS::STRCATRESPONSE")
  (make-package "LECTURE1_SRVS::STRCATRESPONSE"))

(in-package "ROS")





(defclass lecture1_srvs::StrCatRequest
  :super ros::object
  :slots (_first _second ))

(defmethod lecture1_srvs::StrCatRequest
  (:init
   (&key
    ((:first __first) "")
    ((:second __second) "")
    )
   (send-super :init)
   (setq _first (string __first))
   (setq _second (string __second))
   self)
  (:first
   (&optional __first)
   (if __first (setq _first __first)) _first)
  (:second
   (&optional __second)
   (if __second (setq _second __second)) _second)
  (:serialization-length
   ()
   (+
    ;; string _first
    4 (length _first)
    ;; string _second
    4 (length _second)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _first
       (write-long (length _first) s) (princ _first s)
     ;; string _second
       (write-long (length _second) s) (princ _second s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _first
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _first (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _second
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _second (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass lecture1_srvs::StrCatResponse
  :super ros::object
  :slots (_result ))

(defmethod lecture1_srvs::StrCatResponse
  (:init
   (&key
    ((:result __result) "")
    )
   (send-super :init)
   (setq _result (string __result))
   self)
  (:result
   (&optional __result)
   (if __result (setq _result __result)) _result)
  (:serialization-length
   ()
   (+
    ;; string _result
    4 (length _result)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _result
       (write-long (length _result) s) (princ _result s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _result
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _result (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass lecture1_srvs::StrCat
  :super ros::object
  :slots ())

(setf (get lecture1_srvs::StrCat :md5sum-) "1de6aa564a5371132e0030dd77fba176")
(setf (get lecture1_srvs::StrCat :datatype-) "lecture1_srvs/StrCat")
(setf (get lecture1_srvs::StrCat :request) lecture1_srvs::StrCatRequest)
(setf (get lecture1_srvs::StrCat :response) lecture1_srvs::StrCatResponse)

(defmethod lecture1_srvs::StrCatRequest
  (:response () (instance lecture1_srvs::StrCatResponse :init)))

(setf (get lecture1_srvs::StrCatRequest :md5sum-) "1de6aa564a5371132e0030dd77fba176")
(setf (get lecture1_srvs::StrCatRequest :datatype-) "lecture1_srvs/StrCatRequest")
(setf (get lecture1_srvs::StrCatRequest :definition-)
      "string first
string second
---
string result


")

(setf (get lecture1_srvs::StrCatResponse :md5sum-) "1de6aa564a5371132e0030dd77fba176")
(setf (get lecture1_srvs::StrCatResponse :datatype-) "lecture1_srvs/StrCatResponse")
(setf (get lecture1_srvs::StrCatResponse :definition-)
      "string first
string second
---
string result


")



(provide :lecture1_srvs/StrCat "1de6aa564a5371132e0030dd77fba176")


