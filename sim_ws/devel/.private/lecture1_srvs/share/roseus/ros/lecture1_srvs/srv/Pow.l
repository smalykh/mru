;; Auto-generated. Do not edit!


(when (boundp 'lecture1_srvs::Pow)
  (if (not (find-package "LECTURE1_SRVS"))
    (make-package "LECTURE1_SRVS"))
  (shadow 'Pow (find-package "LECTURE1_SRVS")))
(unless (find-package "LECTURE1_SRVS::POW")
  (make-package "LECTURE1_SRVS::POW"))
(unless (find-package "LECTURE1_SRVS::POWREQUEST")
  (make-package "LECTURE1_SRVS::POWREQUEST"))
(unless (find-package "LECTURE1_SRVS::POWRESPONSE")
  (make-package "LECTURE1_SRVS::POWRESPONSE"))

(in-package "ROS")





(defclass lecture1_srvs::PowRequest
  :super ros::object
  :slots (_base _pow ))

(defmethod lecture1_srvs::PowRequest
  (:init
   (&key
    ((:base __base) 0)
    ((:pow __pow) 0)
    )
   (send-super :init)
   (setq _base (round __base))
   (setq _pow (round __pow))
   self)
  (:base
   (&optional __base)
   (if __base (setq _base __base)) _base)
  (:pow
   (&optional __pow)
   (if __pow (setq _pow __pow)) _pow)
  (:serialization-length
   ()
   (+
    ;; int32 _base
    4
    ;; int32 _pow
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _base
       (write-long _base s)
     ;; int32 _pow
       (write-long _pow s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _base
     (setq _base (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _pow
     (setq _pow (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(defclass lecture1_srvs::PowResponse
  :super ros::object
  :slots (_res ))

(defmethod lecture1_srvs::PowResponse
  (:init
   (&key
    ((:res __res) 0)
    )
   (send-super :init)
   (setq _res (round __res))
   self)
  (:res
   (&optional __res)
   (if __res (setq _res __res)) _res)
  (:serialization-length
   ()
   (+
    ;; int32 _res
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _res
       (write-long _res s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _res
     (setq _res (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(defclass lecture1_srvs::Pow
  :super ros::object
  :slots ())

(setf (get lecture1_srvs::Pow :md5sum-) "1e728239f278cabac9e5f35d4114fa74")
(setf (get lecture1_srvs::Pow :datatype-) "lecture1_srvs/Pow")
(setf (get lecture1_srvs::Pow :request) lecture1_srvs::PowRequest)
(setf (get lecture1_srvs::Pow :response) lecture1_srvs::PowResponse)

(defmethod lecture1_srvs::PowRequest
  (:response () (instance lecture1_srvs::PowResponse :init)))

(setf (get lecture1_srvs::PowRequest :md5sum-) "1e728239f278cabac9e5f35d4114fa74")
(setf (get lecture1_srvs::PowRequest :datatype-) "lecture1_srvs/PowRequest")
(setf (get lecture1_srvs::PowRequest :definition-)
      "int32 base
int32 pow
---
int32 res


")

(setf (get lecture1_srvs::PowResponse :md5sum-) "1e728239f278cabac9e5f35d4114fa74")
(setf (get lecture1_srvs::PowResponse :datatype-) "lecture1_srvs/PowResponse")
(setf (get lecture1_srvs::PowResponse :definition-)
      "int32 base
int32 pow
---
int32 res


")



(provide :lecture1_srvs/Pow "1e728239f278cabac9e5f35d4114fa74")


