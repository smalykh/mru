
(cl:in-package :asdf)

(defsystem "lecture1_srvs-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Pow" :depends-on ("_package_Pow"))
    (:file "_package_Pow" :depends-on ("_package"))
  ))