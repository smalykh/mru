; Auto-generated. Do not edit!


(cl:in-package lecture1_srvs-srv)


;//! \htmlinclude Pow-request.msg.html

(cl:defclass <Pow-request> (roslisp-msg-protocol:ros-message)
  ((base
    :reader base
    :initarg :base
    :type cl:integer
    :initform 0)
   (pow
    :reader pow
    :initarg :pow
    :type cl:integer
    :initform 0))
)

(cl:defclass Pow-request (<Pow-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Pow-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Pow-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name lecture1_srvs-srv:<Pow-request> is deprecated: use lecture1_srvs-srv:Pow-request instead.")))

(cl:ensure-generic-function 'base-val :lambda-list '(m))
(cl:defmethod base-val ((m <Pow-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lecture1_srvs-srv:base-val is deprecated.  Use lecture1_srvs-srv:base instead.")
  (base m))

(cl:ensure-generic-function 'pow-val :lambda-list '(m))
(cl:defmethod pow-val ((m <Pow-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lecture1_srvs-srv:pow-val is deprecated.  Use lecture1_srvs-srv:pow instead.")
  (pow m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Pow-request>) ostream)
  "Serializes a message object of type '<Pow-request>"
  (cl:let* ((signed (cl:slot-value msg 'base)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'pow)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Pow-request>) istream)
  "Deserializes a message object of type '<Pow-request>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'base) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'pow) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Pow-request>)))
  "Returns string type for a service object of type '<Pow-request>"
  "lecture1_srvs/PowRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Pow-request)))
  "Returns string type for a service object of type 'Pow-request"
  "lecture1_srvs/PowRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Pow-request>)))
  "Returns md5sum for a message object of type '<Pow-request>"
  "1e728239f278cabac9e5f35d4114fa74")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Pow-request)))
  "Returns md5sum for a message object of type 'Pow-request"
  "1e728239f278cabac9e5f35d4114fa74")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Pow-request>)))
  "Returns full string definition for message of type '<Pow-request>"
  (cl:format cl:nil "int32 base~%int32 pow~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Pow-request)))
  "Returns full string definition for message of type 'Pow-request"
  (cl:format cl:nil "int32 base~%int32 pow~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Pow-request>))
  (cl:+ 0
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Pow-request>))
  "Converts a ROS message object to a list"
  (cl:list 'Pow-request
    (cl:cons ':base (base msg))
    (cl:cons ':pow (pow msg))
))
;//! \htmlinclude Pow-response.msg.html

(cl:defclass <Pow-response> (roslisp-msg-protocol:ros-message)
  ((res
    :reader res
    :initarg :res
    :type cl:integer
    :initform 0))
)

(cl:defclass Pow-response (<Pow-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Pow-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Pow-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name lecture1_srvs-srv:<Pow-response> is deprecated: use lecture1_srvs-srv:Pow-response instead.")))

(cl:ensure-generic-function 'res-val :lambda-list '(m))
(cl:defmethod res-val ((m <Pow-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lecture1_srvs-srv:res-val is deprecated.  Use lecture1_srvs-srv:res instead.")
  (res m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Pow-response>) ostream)
  "Serializes a message object of type '<Pow-response>"
  (cl:let* ((signed (cl:slot-value msg 'res)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Pow-response>) istream)
  "Deserializes a message object of type '<Pow-response>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'res) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Pow-response>)))
  "Returns string type for a service object of type '<Pow-response>"
  "lecture1_srvs/PowResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Pow-response)))
  "Returns string type for a service object of type 'Pow-response"
  "lecture1_srvs/PowResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Pow-response>)))
  "Returns md5sum for a message object of type '<Pow-response>"
  "1e728239f278cabac9e5f35d4114fa74")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Pow-response)))
  "Returns md5sum for a message object of type 'Pow-response"
  "1e728239f278cabac9e5f35d4114fa74")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Pow-response>)))
  "Returns full string definition for message of type '<Pow-response>"
  (cl:format cl:nil "int32 res~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Pow-response)))
  "Returns full string definition for message of type 'Pow-response"
  (cl:format cl:nil "int32 res~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Pow-response>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Pow-response>))
  "Converts a ROS message object to a list"
  (cl:list 'Pow-response
    (cl:cons ':res (res msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'Pow)))
  'Pow-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'Pow)))
  'Pow-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Pow)))
  "Returns string type for a service object of type '<Pow>"
  "lecture1_srvs/Pow")