; Auto-generated. Do not edit!


(cl:in-package lecture1_srvs-srv)


;//! \htmlinclude StrCat-request.msg.html

(cl:defclass <StrCat-request> (roslisp-msg-protocol:ros-message)
  ((first
    :reader first
    :initarg :first
    :type cl:string
    :initform "")
   (second
    :reader second
    :initarg :second
    :type cl:string
    :initform ""))
)

(cl:defclass StrCat-request (<StrCat-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <StrCat-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'StrCat-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name lecture1_srvs-srv:<StrCat-request> is deprecated: use lecture1_srvs-srv:StrCat-request instead.")))

(cl:ensure-generic-function 'first-val :lambda-list '(m))
(cl:defmethod first-val ((m <StrCat-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lecture1_srvs-srv:first-val is deprecated.  Use lecture1_srvs-srv:first instead.")
  (first m))

(cl:ensure-generic-function 'second-val :lambda-list '(m))
(cl:defmethod second-val ((m <StrCat-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lecture1_srvs-srv:second-val is deprecated.  Use lecture1_srvs-srv:second instead.")
  (second m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <StrCat-request>) ostream)
  "Serializes a message object of type '<StrCat-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'first))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'first))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'second))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'second))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <StrCat-request>) istream)
  "Deserializes a message object of type '<StrCat-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'first) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'first) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'second) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'second) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<StrCat-request>)))
  "Returns string type for a service object of type '<StrCat-request>"
  "lecture1_srvs/StrCatRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'StrCat-request)))
  "Returns string type for a service object of type 'StrCat-request"
  "lecture1_srvs/StrCatRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<StrCat-request>)))
  "Returns md5sum for a message object of type '<StrCat-request>"
  "1de6aa564a5371132e0030dd77fba176")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'StrCat-request)))
  "Returns md5sum for a message object of type 'StrCat-request"
  "1de6aa564a5371132e0030dd77fba176")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<StrCat-request>)))
  "Returns full string definition for message of type '<StrCat-request>"
  (cl:format cl:nil "string first~%string second~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'StrCat-request)))
  "Returns full string definition for message of type 'StrCat-request"
  (cl:format cl:nil "string first~%string second~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <StrCat-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'first))
     4 (cl:length (cl:slot-value msg 'second))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <StrCat-request>))
  "Converts a ROS message object to a list"
  (cl:list 'StrCat-request
    (cl:cons ':first (first msg))
    (cl:cons ':second (second msg))
))
;//! \htmlinclude StrCat-response.msg.html

(cl:defclass <StrCat-response> (roslisp-msg-protocol:ros-message)
  ((result
    :reader result
    :initarg :result
    :type cl:string
    :initform ""))
)

(cl:defclass StrCat-response (<StrCat-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <StrCat-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'StrCat-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name lecture1_srvs-srv:<StrCat-response> is deprecated: use lecture1_srvs-srv:StrCat-response instead.")))

(cl:ensure-generic-function 'result-val :lambda-list '(m))
(cl:defmethod result-val ((m <StrCat-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lecture1_srvs-srv:result-val is deprecated.  Use lecture1_srvs-srv:result instead.")
  (result m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <StrCat-response>) ostream)
  "Serializes a message object of type '<StrCat-response>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'result))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'result))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <StrCat-response>) istream)
  "Deserializes a message object of type '<StrCat-response>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'result) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'result) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<StrCat-response>)))
  "Returns string type for a service object of type '<StrCat-response>"
  "lecture1_srvs/StrCatResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'StrCat-response)))
  "Returns string type for a service object of type 'StrCat-response"
  "lecture1_srvs/StrCatResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<StrCat-response>)))
  "Returns md5sum for a message object of type '<StrCat-response>"
  "1de6aa564a5371132e0030dd77fba176")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'StrCat-response)))
  "Returns md5sum for a message object of type 'StrCat-response"
  "1de6aa564a5371132e0030dd77fba176")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<StrCat-response>)))
  "Returns full string definition for message of type '<StrCat-response>"
  (cl:format cl:nil "string result~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'StrCat-response)))
  "Returns full string definition for message of type 'StrCat-response"
  (cl:format cl:nil "string result~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <StrCat-response>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'result))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <StrCat-response>))
  "Converts a ROS message object to a list"
  (cl:list 'StrCat-response
    (cl:cons ':result (result msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'StrCat)))
  'StrCat-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'StrCat)))
  'StrCat-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'StrCat)))
  "Returns string type for a service object of type '<StrCat>"
  "lecture1_srvs/StrCat")