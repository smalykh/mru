#!/usr/bin/env python
import rospy
import tf_conversions
import tf2_ros
import geometry_msgs.msg
import math

from dynamic_reconfigure.server import Server
from lecture2_tf2_py.cfg import DRConfig

X = 0.1
Y = 0.2
Z = 0.3
A = 0.5


def callback(config, level): 
    global R 
    global angle_x
    global angle_y
    global angle_z
    
    R = config.rad
    angle_x = config.anglex
    angle_y = config.angley
    angle_z = config.anglez
    return config

def publisher():
    rospy.init_node("publisher")
    srv = Server(DRConfig, callback)
    rate = rospy.Rate(30)
   
    global R 
    global angle_x
    global angle_y
    global angle_z
    
    R = 1
    angle_x = 0
    angle_y = 0
    angle_z = 0
    
    stb = tf2_ros.StaticTransformBroadcaster()
    sts = geometry_msgs.msg.TransformStamped()
    sts.header.stamp = rospy.Time.now()
    sts.header.frame_id = "base_link"
    sts.child_frame_id = "head"

    sts.transform.translation.x = X
    sts.transform.translation.y = Y
    sts.transform.translation.z = Z

    sq = tf_conversions.transformations.quaternion_from_euler(0, 0, 0)

    sts.transform.rotation.x = sq[0]
    sts.transform.rotation.y = sq[1]
    sts.transform.rotation.z = sq[2]
    sts.transform.rotation.w = sq[3]

    stb.sendTransform(sts)

    tb = tf2_ros.TransformBroadcaster()
    alpha = 0.0
    while not rospy.is_shutdown():
        ts = geometry_msgs.msg.TransformStamped()
     
        ts.header.stamp = rospy.Time.now()
        ts.header.frame_id = "world"
        ts.child_frame_id = "base_link"

        ts.transform.translation.x = R * math.cos(alpha)
        ts.transform.translation.y = R * math.sin(alpha)
        ts.transform.translation.z = 0.0

        q = tf_conversions.transformations.quaternion_from_euler(angle_x, angle_y, angle_z)

        ts.transform.rotation.x = q[0]
        ts.transform.rotation.y = q[1]
        ts.transform.rotation.z = q[2]
        ts.transform.rotation.w = q[3]
        tb.sendTransform(ts)
        rate.sleep()
        alpha += 0.01 * math.pi;

if __name__ == '__main__':
    try:
        publisher()
    except rospy.ROSInterruptException:
        pass

