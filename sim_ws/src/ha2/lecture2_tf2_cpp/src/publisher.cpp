#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <dynamic_reconfigure/server.h>
#include <lecture2_tf2_cpp/DRConfig.h>

static double X = 0.1;
static double Y = 0.2;
static double Z = 0.3;
static double A = 0.5;

static volatile double R = 1.0;
static double angle_x = 0.0;
static double angle_y = 0.0;
static double angle_z = 0.0;

void callback(lecture2_dr_cpp::DRConfig &config, uint32_t level)
{
    R = config.rad;
    angle_x = config.anglex;
    angle_y = config.angley;
    angle_z = config.anglez;
}
int main(int argc, char *argv[])
{
    ros::init(argc, argv, "publisher");
 
    ros::NodeHandle n;
    ros::Rate rate(30);   

    tf2_ros::StaticTransformBroadcaster stb;
    geometry_msgs::TransformStamped sts;

    sts.header.stamp = ros::Time::now();
    sts.header.frame_id = "base_link";
    sts.child_frame_id = "head";
 
    sts.transform.translation.x = X;
    sts.transform.translation.y = Y;
    sts.transform.translation.z = Z;

    tf2::Quaternion sq;
    sq.setRPY(0, 0, 0);
    
    sts.transform.rotation.x = sq.x();
    sts.transform.rotation.y = sq.y();
    sts.transform.rotation.z = sq.z();
    sts.transform.rotation.w = sq.w();
  
    stb.sendTransform(sts);

    dynamic_reconfigure::Server<lecture2_dr_cpp::DRConfig> server;
    server.setCallback(boost::bind(&callback, _1, _2));

    tf2_ros::TransformBroadcaster tb;
    double alpha = 0.0;
    while (ros::ok())
    {
        geometry_msgs::TransformStamped ts;

        ts.header.stamp = ros::Time::now();
        ts.header.frame_id = "world";
        ts.child_frame_id = "base_link";

        ts.transform.translation.x = R * cos(alpha);
        ts.transform.translation.y = R * sin(alpha);
        ts.transform.translation.z = 0.0;

        tf2::Quaternion q;
        q.setRPY((double)angle_x, (double)angle_y, (double)angle_z);

        ts.transform.rotation.x = q.x();
        ts.transform.rotation.y = q.y();
        ts.transform.rotation.z = q.z();
        ts.transform.rotation.w = q.w();

        tb.sendTransform(ts);
        rate.sleep();
    	ros::spinOnce();
        alpha += 0.01 * M_PI;
    }
    return 0;
}

