#include <ros/ros.h>
#include <std_srvs/Trigger.h>

bool callback(std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &resp)
{
    resp.success = 1;
    resp.message = "All works with no play...";
    return true;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "server");
    ros::NodeHandle n;
    ros::ServiceServer s = n.advertiseService("service", callback);
    ros::spin();
    return 0;
}

