#include <ros/ros.h>
#include "std_msgs/String.h"
#include <sstream>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "publisher");
    ros::NodeHandle n;
    ros::Publisher p = n.advertise<std_msgs::String>("topic", 5);
    ros::Rate rate(1);
    int c = 0;
    while (ros::ok())
    {
        std_msgs::String msg;
	
	// Sadly, no...
        //sprintf(msg.data, "%d", c++);
	
	std::ostringstream os;
	os << c++;
	msg.data = os.str();
	
	p.publish(msg);
        rate.sleep();
    }
    return 0;
}

