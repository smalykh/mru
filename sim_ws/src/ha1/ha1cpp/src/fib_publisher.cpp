#include <ros/ros.h>
#include <lecture1_msgs/Fib.h>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "publisher");
    ros::NodeHandle n;
    ros::Publisher p = n.advertise<lecture1_msgs::Fib>("topic", 5);
    ros::Rate rate(1);

    int prev0 = 1, prev1 = 1;
    while (ros::ok())
    {
	int res = prev0 + prev1;
	prev0 = prev1;
	prev1 = res;

        lecture1_msgs::Fib msg;
	msg.data = res;
	
	p.publish(msg);
        rate.sleep();
    }
    return 0;
}

