#include <ros/ros.h>
#include <lecture1_srvs/Pow.h>

bool callback(lecture1_srvs::Pow::Request &req, lecture1_srvs::Pow::Response &resp)
{
    int res = 1;
    for(int i = 0; i < req.pow; i++) 
	res *= req.base;

    resp.res = res;
    return true;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "server");
    ros::NodeHandle n;
    ros::ServiceServer s = n.advertiseService("service", callback);
    ros::spin();
    return 0;
}

