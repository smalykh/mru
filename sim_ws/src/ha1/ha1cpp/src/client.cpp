#include <ros/ros.h>
#include <std_srvs/Trigger.h>

int main(int argc, char *argv[])
{
	ros::init(argc, argv, "client", ros::init_options::AnonymousName);
	ros::NodeHandle n;
	ros::service::waitForService("service", -1);
	ros::ServiceClient c = n.serviceClient<std_srvs::Trigger>("service");
	std_srvs::Trigger srv;
	if (c.call(srv))
        {
		ROS_INFO_STREAM("bool success: " << (srv.response.success ? "True" : "False"));
		ROS_INFO_STREAM("string message: " << srv.response.message);
        }
        else
        {	
		ROS_ERROR("Service call failed");
        }
	return 0;
}

