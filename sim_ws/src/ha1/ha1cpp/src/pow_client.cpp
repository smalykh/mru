#include <ros/ros.h>
#include <lecture1_srvs/Pow.h>

int main(int argc, char *argv[])
{
	ros::init(argc, argv, "client", ros::init_options::AnonymousName);
	ros::NodeHandle n;
	ros::service::waitForService("service", -1);
	ros::ServiceClient c = n.serviceClient<lecture1_srvs::Pow>("service");
	// Print all power of 2 from 0 to 10
	for (int i = 0; i < 10; i++)
	{
		lecture1_srvs::Pow srv;
		srv.request.base = 2;
		srv.request.pow = i;

		if (c.call(srv))
	       	{
			ROS_INFO_STREAM("Result: " << srv.response.res);
       		}
       		else
       		{	
			ROS_ERROR("Service call failed");
        	}
	}
	return 0;
}

