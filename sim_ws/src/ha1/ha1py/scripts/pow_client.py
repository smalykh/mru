#!/usr/bin/env python
import rospy
from lecture1_srvs.srv import Pow

def client():
    rospy.wait_for_service("service")
    for i in range(10):
        try:
            sp = rospy.ServiceProxy("service", Pow)
            resp = sp(2, i)
            print "Result: " + str(resp.res)
        except rospy.ServiceException, e:
            print "Service call failed: " + str(e)

if __name__ == "__main__":
    client()

