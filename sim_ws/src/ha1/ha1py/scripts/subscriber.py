#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def callback(msg):
    rospy.loginfo("%s", msg.data)
    
def subscriber():
    rospy.init_node("subscriber", anonymous=True)
    rospy.Subscriber("topic", String, callback)
    rospy.spin()

if __name__ == '__main__':
    subscriber()

