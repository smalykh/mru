#!/usr/bin/env python
import rospy
from std_srvs.srv import Trigger

def client():
    rospy.wait_for_service("service")
    try:
        sp = rospy.ServiceProxy("service", Trigger)
        resp = sp()
        print "bool success: " + str(resp.success)
        print "string message: " + resp.message
    except rospy.ServiceException, e:
        print "Service call failed: " + str(e)

if __name__ == "__main__":
    client()

