#!/usr/bin/env python
import rospy
from lecture1_msgs.msg import Fib

def callback(msg):
    rospy.loginfo("%d", msg.data)
    
def subscriber():
    rospy.init_node("subscriber", anonymous=True)
    rospy.Subscriber("topic", Fib, callback)
    rospy.spin()

if __name__ == '__main__':
    subscriber()

