#!/usr/bin/env python
import rospy
from lecture1_srvs.srv import Pow, PowResponse

def callback(req):
    res = 1
    for i in range(req.pow):
        res *= req.base;
    return PowResponse(res)

def server():
    rospy.init_node("server")
    s = rospy.Service("service", Pow, callback)
    rospy.spin()

if __name__ == "__main__":
    server()
