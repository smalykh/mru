#!/usr/bin/env python
import rospy
from std_srvs.srv import Trigger, TriggerResponse

def callback(req):
    return TriggerResponse(1, "All works with no play")

def server():
    rospy.init_node("server")
    s = rospy.Service("service", Trigger, callback)
    rospy.spin()

if __name__ == "__main__":
    server()
