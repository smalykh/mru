#!/usr/bin/env python
import rospy
from lecture1_msgs.msg import Fib

def publisher():
    rospy.init_node("publisher")
    p = rospy.Publisher("topic", Fib, queue_size=5)
    rate = rospy.Rate(1)
    cprev0 = 1
    cprev1 = 1
    while not rospy.is_shutdown():
	data = cprev0 + cprev1
	cprev0 = cprev1
	cprev1 = data

	p.publish(data)
        rate.sleep()

if __name__ == '__main__':
    try:
        publisher()
    except rospy.ROSInterruptException:
        pass


