#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/sergey/Apps/mru/sim_ws/devel/.private/hbot_description:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/sergey/Apps/mru/sim_ws/devel/.private/hbot_description/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/sergey/Apps/mru/sim_ws/devel/.private/hbot_description/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/sergey/Apps/mru/sim_ws/build/hbot_description"
export ROSLISP_PACKAGE_DIRECTORIES="/home/sergey/Apps/mru/sim_ws/devel/.private/hbot_description/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/sergey/Apps/mru/sim_ws/src/ha3/hbot_description:$ROS_PACKAGE_PATH"