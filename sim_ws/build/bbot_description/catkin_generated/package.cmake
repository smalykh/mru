set(_CATKIN_CURRENT_PACKAGE "bbot_description")
set(bbot_description_VERSION "0.0.1")
set(bbot_description_MAINTAINER "Alexander Gridnev <aleksgridnev@gmail.com>")
set(bbot_description_PACKAGE_FORMAT "2")
set(bbot_description_BUILD_DEPENDS )
set(bbot_description_BUILD_EXPORT_DEPENDS )
set(bbot_description_BUILDTOOL_DEPENDS "catkin")
set(bbot_description_BUILDTOOL_EXPORT_DEPENDS )
set(bbot_description_EXEC_DEPENDS )
set(bbot_description_RUN_DEPENDS )
set(bbot_description_TEST_DEPENDS )
set(bbot_description_DOC_DEPENDS )
set(bbot_description_URL_WEBSITE "")
set(bbot_description_URL_BUGTRACKER "")
set(bbot_description_URL_REPOSITORY "")
set(bbot_description_DEPRECATED "")