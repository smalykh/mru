set(_CATKIN_CURRENT_PACKAGE "lecture2_action_py")
set(lecture2_action_py_VERSION "0.0.0")
set(lecture2_action_py_MAINTAINER "alex <alex@todo.todo>")
set(lecture2_action_py_PACKAGE_FORMAT "2")
set(lecture2_action_py_BUILD_DEPENDS "lecture2_actions" "rospy")
set(lecture2_action_py_BUILD_EXPORT_DEPENDS "lecture2_actions" "rospy")
set(lecture2_action_py_BUILDTOOL_DEPENDS "catkin")
set(lecture2_action_py_BUILDTOOL_EXPORT_DEPENDS )
set(lecture2_action_py_EXEC_DEPENDS "lecture2_actions" "rospy")
set(lecture2_action_py_RUN_DEPENDS "lecture2_actions" "rospy")
set(lecture2_action_py_TEST_DEPENDS )
set(lecture2_action_py_DOC_DEPENDS )
set(lecture2_action_py_URL_WEBSITE "")
set(lecture2_action_py_URL_BUGTRACKER "")
set(lecture2_action_py_URL_REPOSITORY "")
set(lecture2_action_py_DEPRECATED "")