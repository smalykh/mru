# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "lecture1_srvs: 0 messages, 1 services")

set(MSG_I_FLAGS "")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(lecture1_srvs_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv" NAME_WE)
add_custom_target(_lecture1_srvs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "lecture1_srvs" "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages

### Generating Services
_generate_srv_cpp(lecture1_srvs
  "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/lecture1_srvs
)

### Generating Module File
_generate_module_cpp(lecture1_srvs
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/lecture1_srvs
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(lecture1_srvs_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(lecture1_srvs_generate_messages lecture1_srvs_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv" NAME_WE)
add_dependencies(lecture1_srvs_generate_messages_cpp _lecture1_srvs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(lecture1_srvs_gencpp)
add_dependencies(lecture1_srvs_gencpp lecture1_srvs_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS lecture1_srvs_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages

### Generating Services
_generate_srv_eus(lecture1_srvs
  "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/lecture1_srvs
)

### Generating Module File
_generate_module_eus(lecture1_srvs
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/lecture1_srvs
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(lecture1_srvs_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(lecture1_srvs_generate_messages lecture1_srvs_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv" NAME_WE)
add_dependencies(lecture1_srvs_generate_messages_eus _lecture1_srvs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(lecture1_srvs_geneus)
add_dependencies(lecture1_srvs_geneus lecture1_srvs_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS lecture1_srvs_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages

### Generating Services
_generate_srv_lisp(lecture1_srvs
  "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/lecture1_srvs
)

### Generating Module File
_generate_module_lisp(lecture1_srvs
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/lecture1_srvs
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(lecture1_srvs_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(lecture1_srvs_generate_messages lecture1_srvs_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv" NAME_WE)
add_dependencies(lecture1_srvs_generate_messages_lisp _lecture1_srvs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(lecture1_srvs_genlisp)
add_dependencies(lecture1_srvs_genlisp lecture1_srvs_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS lecture1_srvs_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages

### Generating Services
_generate_srv_nodejs(lecture1_srvs
  "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/lecture1_srvs
)

### Generating Module File
_generate_module_nodejs(lecture1_srvs
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/lecture1_srvs
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(lecture1_srvs_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(lecture1_srvs_generate_messages lecture1_srvs_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv" NAME_WE)
add_dependencies(lecture1_srvs_generate_messages_nodejs _lecture1_srvs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(lecture1_srvs_gennodejs)
add_dependencies(lecture1_srvs_gennodejs lecture1_srvs_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS lecture1_srvs_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages

### Generating Services
_generate_srv_py(lecture1_srvs
  "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lecture1_srvs
)

### Generating Module File
_generate_module_py(lecture1_srvs
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lecture1_srvs
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(lecture1_srvs_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(lecture1_srvs_generate_messages lecture1_srvs_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sergey/Apps/mru/sim_ws/src/ha1/lecture1_srvs/srv/Pow.srv" NAME_WE)
add_dependencies(lecture1_srvs_generate_messages_py _lecture1_srvs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(lecture1_srvs_genpy)
add_dependencies(lecture1_srvs_genpy lecture1_srvs_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS lecture1_srvs_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/lecture1_srvs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/lecture1_srvs
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/lecture1_srvs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/lecture1_srvs
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/lecture1_srvs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/lecture1_srvs
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/lecture1_srvs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/lecture1_srvs
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lecture1_srvs)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lecture1_srvs\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/lecture1_srvs
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
