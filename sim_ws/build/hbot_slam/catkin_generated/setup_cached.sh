#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/sergey/Apps/mru/sim_ws/devel/.private/hbot_slam:$CMAKE_PREFIX_PATH"
export PWD="/home/sergey/Apps/mru/sim_ws/build/hbot_slam"
export ROSLISP_PACKAGE_DIRECTORIES="/home/sergey/Apps/mru/sim_ws/devel/.private/hbot_slam/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/sergey/Apps/mru/sim_ws/src/ha4/hbot_slam:$ROS_PACKAGE_PATH"