# CMake generated Testfile for 
# Source directory: /home/sergey/Apps/mru/sim_ws/src/ha3
# Build directory: /home/sergey/Apps/mru/sim_ws/build/turtlebot_description
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_turtlebot_description_gtest_turtlebot_description_test_urdf "/home/sergey/Apps/mru/sim_ws/build/turtlebot_description/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/sergey/Apps/mru/sim_ws/build/turtlebot_description/test_results/turtlebot_description/gtest-turtlebot_description_test_urdf.xml" "--return-code" "/home/sergey/Apps/mru/sim_ws/devel/.private/turtlebot_description/lib/turtlebot_description/turtlebot_description_test_urdf --gtest_output=xml:/home/sergey/Apps/mru/sim_ws/build/turtlebot_description/test_results/turtlebot_description/gtest-turtlebot_description_test_urdf.xml")
subdirs(gtest)
